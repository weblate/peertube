import Vue from 'vue'
import App from './App.vue'
import i18next from 'i18next';
import VueI18Next from '@panter/vue-i18next';
import Fetch from 'i18next-fetch-backend';
import LanguageDetector from 'i18next-browser-languagedetector';

Vue.use(VueI18Next);
i18next
    .use(Fetch)
    .use(LanguageDetector)
    .init({
        fallbackLng: 'en',
        whitelist: [
            'en',
            'fr'
        ],
        keySeparator: '•',
        nsSeparator: 'Ŏ',
        lowerCaseLng: true,
        interpolation: {
            defaultVariables: {
                endtag: '</a>'
            }
        },
        backend: {
            loadPath: 'locales/{{lng}}.json'
        }
    })
const i18n = new VueI18Next(i18next);

Vue.config.productionTip = false

new Vue({
    render: h => h(App),
    i18n: i18n
}).$mount('#app')
