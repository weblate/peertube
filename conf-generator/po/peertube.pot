msgid ""
msgstr ""
"Project-Id-Version: i18next-conv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"POT-Creation-Date: 2019-11-03T11:58:03.360Z\n"
"PO-Revision-Date: 2019-11-03T11:58:03.360Z\n"
"Language: en\n"

msgid "PeerTube pillar generator"
msgstr "PeerTube pillar generator"

msgid "Created by {{- opentag}}Luc Didry{{- endtag}}"
msgstr "Created by {{- opentag}}Luc Didry{{- endtag}}"

msgid "Source"
msgstr "Source"

msgid "unlimited"
msgstr "unlimited"

msgid "Your instance"
msgstr "Your instance"

msgid "Language"
msgstr "Language"

msgid ""
"Web address of your PeerTube instance (without {{- opentag}}http(s)://{{- "
"endtag}})"
msgstr ""
"Web address of your PeerTube instance (without {{- opentag}}http(s)://{{- "
"endtag}})"

msgid "Name of your instance"
msgstr "Name of your instance"

msgid "Short description of your instance"
msgstr "Short description of your instance"

msgid ""
"Long description of your instance (support {{- opentag}}Markdown{{- "
"endtag}})"
msgstr ""
"Long description of your instance (support {{- opentag}}Markdown{{- "
"endtag}})"

msgid "Terms of use (support {{- opentag}}Markdown{{- endtag}})"
msgstr "Terms of use (support {{- opentag}}Markdown{{- endtag}})"

msgid "Code of conduct (support {{- opentag}}Markdown{{- endtag}})"
msgstr "Code of conduct (support {{- opentag}}Markdown{{- endtag}})"

msgid ""
"Who moderates the instance? What is the policy regarding NSFW videos? "
"Political videos? etc. (support {{- opentag}}Markdown{{- endtag}})"
msgstr ""
"Who moderates the instance? What is the policy regarding NSFW videos? "
"Political videos? etc. (support {{- opentag}}Markdown{{- endtag}})"

msgid "Homepage"
msgstr "Homepage"

msgid "Video overview"
msgstr "Video overview"

msgid "Trending videos"
msgstr "Trending videos"

msgid "Recently added videos"
msgstr "Recently added videos"

msgid "Local videos"
msgstr "Local videos"

msgid ""
"Policy for displaying explicit videos (pornography, violent content…) on "
"pages listing videos (\"Overview\", \"Trend\", \"Recently added\", "
"\"Local\" pages)"
msgstr ""
"Policy for displaying explicit videos (pornography, violent content…) on "
"pages listing videos (\"Overview\", \"Trend\", \"Recently added\", "
"\"Local\" pages)"

msgid "Do not list explicit videos"
msgstr "Do not list explicit videos"

msgid "List but blur the thumbnails"
msgstr "List but blur the thumbnails"

msgid "Normally list explicit videos"
msgstr "Normally list explicit videos"

msgid "Setting up email sending"
msgstr "Setting up email sending"

msgid "Use the email daemon of your PeerTube server"
msgstr "Use the email daemon of your PeerTube server"

msgid "More informations about your instance"
msgstr "More informations about your instance"

msgid "Why did you create this instance?"
msgstr "Why did you create this instance?"

msgid "Who is behind the instance? A single person? A non profit?"
msgstr "Who is behind the instance? A single person? A non profit?"

msgid "How long do you plan to maintain this instance?"
msgstr "How long do you plan to maintain this instance?"

msgid ""
"How will you pay the PeerTube instance server? With you own funds? With "
"users donations? Advertising?"
msgstr ""
"How will you pay the PeerTube instance server? With you own funds? With "
"users donations? Advertising?"

msgid ""
"If you want to explain on what type of hardware your PeerTube instance "
"runs. For ex. “2 vCore, 2GB RAM…”"
msgstr ""
"If you want to explain on what type of hardware your PeerTube instance "
"runs. For ex. “2 vCore, 2GB RAM…”"

msgid "Admin account of your instance"
msgstr "Admin account of your instance"

msgid "Password of the {{- opentag}}root{{- endtag}} account"
msgstr "Password of the {{- opentag}}root{{- endtag}} account"

msgid "Please choose a strong password that you don’t use elsewhere"
msgstr "Please choose a strong password that you don’t use elsewhere"

msgid "Email sending server"
msgstr "Email sending server"

msgid "Port"
msgstr "Port"

msgid "Username"
msgstr "Username"

msgid "Password"
msgstr "Password"

msgid "Use TLS?"
msgstr "Use TLS?"

msgid "Disable StartTLS?"
msgstr "Disable StartTLS?"

msgid "Path to the file of the certification authority"
msgstr "Path to the file of the certification authority"

msgid "Email sender address"
msgstr "Email sender address"

msgid "Searching for videos"
msgstr "Searching for videos"

msgid ""
"Allows to search for videos or accounts by their URL, which may be on "
"instances that are not federated with yours."
msgstr ""
"Allows to search for videos or accounts by their URL, which may be on "
"instances that are not federated with yours."

msgid ""
"When this feature is enabled, those who benefit from this feature will be "
"able to \"escape\" from the instances followed by yours. This means that "
"they will be able to follow channels, read videos or list videos of these "
"instances not followed by yours."
msgstr ""
"When this feature is enabled, those who benefit from this feature will be "
"able to \"escape\" from the instances followed by yours. This means that "
"they will be able to follow channels, read videos or list videos of these "
"instances not followed by yours."

msgid "Allow this for registered users?"
msgstr "Allow this for registered users?"

msgid "Allow this for anonymous visitors?"
msgstr "Allow this for anonymous visitors?"

msgid "Number of days on which trending videos will be calculated"
msgstr "Number of days on which trending videos will be calculated"

msgid "Contact form"
msgstr "Contact form"

msgid "Activate contact form? {{- opentag}}recommended{{- endtag}}"
msgstr "Activate contact form? {{- opentag}}recommended{{- endtag}}"

msgid "Registrations"
msgstr "Registrations"

msgid "Open registrations?"
msgstr "Open registrations?"

msgid ""
"Maximum number of accounts (when this number is reached, registrations are "
"disabled. For an unlimited number of accounts, put {{- opentag}}-1{{- "
"endtag}})"
msgstr ""
"Maximum number of accounts (when this number is reached, registrations are "
"disabled. For an unlimited number of accounts, put {{- opentag}}-1{{- "
"endtag}})"

msgid ""
"Checking the email address? (requires a configured and functional mail "
"server) {{- opentag}}recommended{{- endtag}}"
msgstr ""
"Checking the email address? (requires a configured and functional mail "
"server) {{- opentag}}recommended{{- endtag}}"

msgid "Disk quotas"
msgstr "Disk quotas"

msgid ""
"Default disk quota per user (for an unlimited quota, put {{- opentag}}-1{{- "
"endtag}})"
msgstr ""
"Default disk quota per user (for an unlimited quota, put {{- opentag}}-1{{- "
"endtag}})"

msgid ""
"Default daily disk quota per user (for an unlimited quota, put {{- "
"opentag}}-1{{- endtag}})"
msgstr ""
"Default daily disk quota per user (for an unlimited quota, put {{- "
"opentag}}-1{{- endtag}})"

msgid "Transcoding (videos re-encoding)"
msgstr "Transcoding (videos re-encoding)"

msgid "Activate transcoding? {{- opentag}}recommended{{- endtag}}"
msgstr "Activate transcoding? {{- opentag}}recommended{{- endtag}}"

msgid ""
"Allow additional extensions? This allows your users to upload.mkv, mov… "
"files instead of just .mp4 files. {{- opentag}}recommended{{- endtag}}"
msgstr ""
"Allow additional extensions? This allows your users to upload.mkv, mov… "
"files instead of just .mp4 files. {{- opentag}}recommended{{- endtag}}"

msgid "How many CPU threads should be allocated to transcoding?"
msgstr "How many CPU threads should be allocated to transcoding?"

msgid ""
"Transcoding in several qualities lower than the quality of the original "
"video allows visitors with low bit rates to enjoy the video smoothly."
msgstr ""
"Transcoding in several qualities lower than the quality of the original "
"video allows visitors with low bit rates to enjoy the video smoothly."

msgid "Transcode in {{quality}}p quality? {{- opentag}}recommended{{- endtag}}"
msgstr "Transcode in {{quality}}p quality? {{- opentag}}recommended{{- endtag}}"

msgid "Importing external videos"
msgstr "Importing external videos"

msgid "Allow your users to import remote videos (from YouTube, from torrent…)?"
msgstr "Allow your users to import remote videos (from YouTube, from torrent…)?"

msgid ""
"Allow for videos accessible in HTTP or any site supported by {{- "
"opentag}}youtube-dl{{- endtag}}?"
msgstr ""
"Allow for videos accessible in HTTP or any site supported by {{- "
"opentag}}youtube-dl{{- endtag}}?"

msgid "Allow via torrent (via URI magnet or torrent file)?"
msgstr "Allow via torrent (via URI magnet or torrent file)?"

msgid "Federation"
msgstr "Federation"

msgid "Allow or not other instances to follow yours"
msgstr "Allow or not other instances to follow yours"

msgid "Whether or not an administrator must manually validate a new follower"
msgstr "Whether or not an administrator must manually validate a new follower"

msgid ""
"Apticron is a program that runs daily on your server and notifies you by "
"email if package updates are available."
msgstr ""
"Apticron is a program that runs daily on your server and notifies you by "
"email if package updates are available."

msgid "Install apticron? {{- opentag}}recommended{{- endtag}}"
msgstr "Install apticron? {{- opentag}}recommended{{- endtag}}"

msgid ""
"Recipient for apticron’s emails ({{- opentag}}root{{- endtag}} is ok if you "
"configured a real address in {{- opentag}}/etc/aliases{{- endtag}}"
msgstr ""
"Recipient for apticron’s emails ({{- opentag}}root{{- endtag}} is ok if you "
"configured a real address in {{- opentag}}/etc/aliases{{- endtag}}"

msgid "Download configuration"
msgstr "Download configuration"

msgid "Reset"
msgstr "Reset"