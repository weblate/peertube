peertube:
  my_server:
    db_password: ''
    apticron:
      enabled: false
      email: root
    hostname: peertube.example.com
    root_password: ''
    admin:
      email: admin@example.com
    smtp:
      hostname: ''
      port: 465
      username: ''
      password: ''
      tls: true
      disable_starttls: false
      ca_file: ''
      from_address: admin@example.com
    search:
      remote_uri:
        users: true
        anonymous: false
    trending:
      videos:
        interval_days: 7
    contact_form:
      enabled: true
    signup:
      enabled: false
      limit: 10
      requires_email_verification: false
    user:
      video_quota: -1
      video_quota_daily: -1
    transcoding:
      enabled: true
      allow_additional_extensions: true
      threads: 1
      resolutions:
        240p: false
        360p: false
        480p: false
        720p: false
        1080p: false
        2160p: false
    import:
      videos:
        http:
          enabled: false
        torrent:
          enabled: false
    instance:
      name: 'PeerTube'
      short_description: 'PeerTube, a federated (ActivityPub) video streaming platform using P2P (BitTorrent) directly in the web browser with WebTorrent and Angular.'
      description: ''
      terms: ''
      code_of_conduct: ''
      moderation_information: ''
      creation_reason: ''
      administrator: ''
      maintenance_lifetime: ''
      business_model: ''
      hardware_information: ''
      default_client_route: '/videos/trending'
      default_nsfw_policy: 'do_not_list'
    followers:
      instance:
        enabled: true
        manual_approval: false
