#!/bin/bash
export BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=yes
borg='/usr/bin/borg'
target='/var/www/peertube/daily_backups/borg/'
src='/var/www/peertube/'
exclude='-e /var/www/peertube/daily_backups/borg/ -e /var/www/peertube/storage/tmp/ -e /var/www/peertube/storage/cache/'

## Backup the database
## Dump of peertube_prod database, suitable for pg_restore
pg_dump --format custom -f /var/www/peertube/daily_backups/postgresql/peertube_prod.$(date +%A).bak -d peertube_prod

## Show me the backups
$borg list $target

## Backup Peertube
$borg create -v --stats $exclude $target::peertube-{now:%Y-%m-%d_%H-%M} $src

## Check the backups
$borg check $target

## Prune old backups
$borg prune -v --list --save-space -w 4 -d 10 --prefix='peertube' $target
