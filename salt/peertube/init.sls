{% set settings = pillar['peertube'][grains['id']] %}
##################
## Dependencies ##
##################
dependencies:
  pkg.installed:
    - pkgs:
      - apt-transport-https
      - borgbackup
      - certbot
      - curl
      - ffmpeg
      - git
      - g++
      - make
      - nginx
      - openssl
      - postgresql
      - postgresql-contrib
      - python-dev
      - redis-server
      - unzip
nodejs:
  pkgrepo.managed:
    - name: deb https://deb.nodesource.com/node_10.x {{ grains['oscodename'] }} main
    - file: /etc/apt/sources.list.d/nodesource.list
    - key_url: https://deb.nodesource.com/gpgkey/nodesource.gpg.key
  pkg.installed:
    - name: nodejs
yarn:
  pkgrepo.managed:
    - name: deb https://dl.yarnpkg.com/debian/ stable main
    - file: /etc/apt/sources.list.d/yarn.list
    - key_url: https://dl.yarnpkg.com/debian/pubkey.gpg
  pkg.installed:
    - name: yarn

###################
## User creation ##
###################
peertube_user:
  user.present:
    - name: peertube
    - home: /var/www/peertube
    - createhome: True
    - empty_password: True
    - system: True
  alias.present:
    - target: {{ settings.admin.email }}
  file.append:
    - name: /etc/ssh/sshd_config
    - text: DenyUsers peertube
  service.running:
    - name: ssh
    - enable: True
    - watch:
      - file: /etc/ssh/sshd_config

#########################
## PostgreSQL database ##
#########################
peertube:
  postgres_user.present:
    - password: {{ settings.db_password }}
postgresql_db:
  postgres_database.present:
    - name: peertube_prod
    - owner: peertube
# PostgreSQL extensions
{% for ext in ['pg_trgm', 'unaccent'] %}
postgresql_{{ext}}:
  postgres_extension.present:
    - name: {{ext}}
    - user: postgres
    - maintenance_db: peertube_prod
{% endfor %}

#################
## Directories ##
#################
{% for dir in ['config', 'storage', 'versions'] %}
/var/www/peertube/{{dir}}:
  file.directory:
    - user: peertube
    - group: peertube
    - dir_mode: 755
{% endfor %}

######################
## Install Peertube ##
######################
salt://peertube/files/get_peertube.sh:
  cmd.script:
    - runas: peertube
    - shell: /bin/bash

########################
## Configure Peertube ##
########################
/var/www/peertube/config/production.yaml:
  file.managed:
    - source: salt://peertube/files/production.yaml.sls
    - user: peertube
    - group: peertube
    - mode: 640
    - template: jinja
    - defaults:
        settings: {{ settings }}

############################################
## Put Peertube logs in /var/log/peertube ##
############################################
/var/log/peertube:
  file.directory:
    - user: root
    - group: adm
    - mode: 750
/etc/logrotate.d/peertube.conf:
  file.managed:
    - source: salt://peertube/files/logrotate.conf
/etc/rsyslog.d/peertube.conf:
  file.managed:
    - contents:
      - if $programname == 'peertube' then /var/log/peertube/peertube.log
      - if $programname == 'peertube' then ~
  service.running:
    - name: rsyslog
    - enable: True
    - watch:
      - file: /etc/rsyslog.d/peertube.conf

###############################
## Enable and start Peertube ##
###############################
salt://peertube/files/systemd.sh:
  cmd.script:
    - shell: /bin/bash

##################################
## Change root account password ##
##################################
change_pt_root_password:
  cmd.run:
    - name: echo {{ settings.root_password }} | NODE_CONFIG_DIR=/var/www/peertube/config NODE_ENV=production npm run reset-password -- -u root
    - cwd: /var/www/peertube/peertube-latest
    - runas: peertube

#########################
## Configure Webserver ##
#########################
/etc/nginx/sites-available/peertube:
  file.managed:
    - source: salt://peertube/files/nginx.conf.sls
    - template: jinja
    - defaults:
        settings: {{ settings }}
/etc/nginx/sites-enabled/peertube:
  file.symlink:
    - target: /etc/nginx/sites-available/peertube
  cmd.run:
    - name: nginx -t && nginx -s reload
letsencrypt:
  cmd.run:
    - name: certbot certonly --non-interactive --rsa-key-size 4096 --webroot -w /var/www/certbot/ --email {{ settings.letsencrypt_email|default(settings.admin.email) }} --agree-tos --text --renew-hook "/usr/sbin/nginx -s reload" -d {{ settings.hostname }}
  file.uncomment:
    - name: /etc/nginx/sites-available/peertube
    - regex: ^#
  service.running:
    - name: nginx
    - enable: True
    - reload: True
    - watch:
      - file: /etc/nginx/sites-available/peertube

############################
## Certificate monitoring ##
############################
certificate_monitoring:
  pkg.installed:
    - name: monitoring-plugins-basic
  cron.present:
    - name: /usr/lib/nagios/plugins/check_http -H {{ settings.hostname }} -C 25 --sni -p 443 -t 15 | grep -v OK
    - user: peertube
    - minute: 0
    - hour: 6
    - comment: Check for the Peertube certificate expiration delay. If warning, try to reload nginx. If still warning, do 'certbot renew'

############
## Backup ##
############
/var/www/peertube/daily_backups:
  file.directory:
    - dir_mode: 755
    - user: root

# PostgreSQL database
/var/www/peertube/daily_backups/postgresql:
  file.directory:
    - dir_mode: 750
    - user: postgres

# Backup with borg
borgbackup:
  file.directory:
    - name: /var/www/peertube/daily_backups/borg
    - dir_mode: 750
  cmd.run:
    - name: borg init -e none /var/www/peertube/daily_backups/borg/
backup_script:
  file.managed:
    - name: /var/www/peertube/daily_backups/backup-peertube.sh
    - source: salt://peertube/files/backup.sh
    - user: root
    - mode: 750
  cron.present:
    - name: /var/www/peertube/daily_backups/backup-peertube.sh
    - user: root
    - comment: Backup Peertube on borg
    - minute: 42
    - hour: 1

{% if settings.apticron is defined and settings.apticron.enabled%}
#############################################
## Let's try to keep the server up to date ##
#############################################
apticron:
  pkg.installed
/etc/apticron/apticron.conf:
  file.replace:
    - name: /etc/apticron/apticron.conf
    - pattern: EMAIL="root"
    - repl: EMAIL="{{ settings.apticron.email }}"
{% endif %}
